Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/ENSTABretagneRobotics/VIBES
Upstream-Name: VIBes
Upstream-Contact: https://github.com/ENSTABretagneRobotics
Files-Excluded: client-api/MATLAB
                viewer/VIBes_DMG_bg

Files: *
Copyright: 2013-2017 Vincent Drevelle
           2013-2017 Jeremy Nicola 
License: GPL-3+
Comment:
 The COPYING file, which contains the terms of the GNU General Public
 Licence, is not present in the tarball, but is included in the
 upstream Git repository.

Files: client-api/C++/*
Copyright: 2013 Luc Jaulin
           2013-2015 Jeremy Nicola
           2013-2015 Vincent Drevelle
           2013-2015 Simon Rohou
           2013-2015 Benoit Desrochers
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: debian/*
Copyright: 2023, 2024 Rafael Laboissière <rafael@debian.org>
           2024 Steve Langasek <vorlon@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

