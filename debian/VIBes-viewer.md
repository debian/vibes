% VIBes-viewer(1) | User Commands
%
% "December 16 2023"

# NAME

VIBes-viewer - Visualizer for Intervals and Boxes

# SYNOPSIS

**VIBes-viewer** 

# DESCRIPTION

This manual page documents briefly the **VIBes-viewer** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page. Instead, it has documentation
in the GNU info(1) format; see below.

VIBes is a visualization system that aims at providing people working
with interval methods a way to display results (boxes, pavings),
without worrying with GUI programming. It provides drawing functions
accessible from a lot of programming languages, without complex
installation and library dependencies. The main design goal of VIBes
is to be cross-platform, available from different programming
languages, simple to set-up, easy to port to a new language.

The **VIBes-viewer** command is the server side of VIBes and features
viewing, annotating and exporting figures. The VIBes API enables your
program to communicate with the viewer in order to draw figures from
different programming languages.

# FILES

${HOME}/.vibes.json
:   File used for communication between the client and the VIBes server

# SEE ALSO

Further information can be found at the VIBes website:
http://enstabretagnerobotics.github.io/VIBES

# COPYRIGHT

Copyright © 2023 Rafael Laboissière

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
